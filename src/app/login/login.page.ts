import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TabsPage } from '../tabs/tabs.page';
import { ApiService } from '../services/api.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';

@Component({
	selector: 'app-login',
	templateUrl: './login.page.html',
	styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

	homePage: TabsPage;
	loginData: any;

	constructor(
		private router: Router,
		public httpClient: HttpClient,
		public apiService: ApiService,
		private storage: Storage) {
		this.loginData = [];

	}

	ngOnInit() {
		this.storage.get("session").then((val) => {
			console.log("Session : " + val);
			if(val != null){
				this.router.navigate(['app/tabs/tab1']);
			}
		});
		// this.getLogin();
	}

	goToHome(){
		this.apiService.getLogin("NXRRdElCZ2lvM3IycmgzM3VKNzBrQT09","test@test.com","123").subscribe(response => {
			this.loginData = response;
			if(response.status == "success"){
				this.router.navigate(['app/tabs/tab1']);
				this.storage.set("session", response.session);
				// console.log(response.result[0].jabatan);
				this.storage.get('session').then((val) => {
					console.log('Your age is', val);
				});
			}
		});
	}

	getLogin(){
		this.apiService.getLogin("NXRRdElCZ2lvM3IycmgzM3VKNzBrQT09","test@test.com","123").subscribe(response => {
			this.loginData = response;
			this.storage.set("session", response.session);
			// console.log(response.result[0].jabatan);
			this.storage.get('session').then((val) => {
				console.log('Your age is', val);
			});
		});
	}

}
