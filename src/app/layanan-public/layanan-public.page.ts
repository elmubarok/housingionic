import { Component, OnInit } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ToastController } from '@ionic/angular';
import { ApiService } from '../services/api.service';

@Component({
	selector: 'app-layanan-public',
	templateUrl: './layanan-public.page.html',
	styleUrls: ['./layanan-public.page.scss'],
})
export class LayananPublicPage implements OnInit {

	curImg = [];
	uid: string = "YUtmVzdtY2NlbVhSTVZLVW1NNktPQT09";
	demoImg = [1,2,3];
	dataBase: string;

	constructor(
		public actionSheetController: ActionSheetController,
		private camera: Camera,
		public toastController: ToastController,
		public apiService: ApiService) { }

	ngOnInit() {
		this.presentToast("Hello");
	}

	async presentToast(pict: string) {
		const toast = await this.toastController.create({
			message: pict,
			duration: 2000
		});
		toast.present();
	}

	async attachPic() {
		const actionSheet = await this.actionSheetController.create({
			header: 'Pilih',
			buttons: [{
				text: 'Take Picture',
				icon: 'camera',
				handler: () => {
					// act
					this.takePicture(this.camera.PictureSourceType.CAMERA);
				}
			}, {
				text: 'Phone Library',
				icon: 'photos',
				handler: () => {
					// act
					this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
				}
			}]
		});
		await actionSheet.present();
	}

	takePicture(sourceType){
		const options: CameraOptions = {
			quality: 100,
			sourceType: sourceType,
			destinationType: this.camera.DestinationType.DATA_URL,
			encodingType: this.camera.EncodingType.JPEG,
			mediaType: this.camera.MediaType.PICTURE
		};

		this.camera.getPicture(options).then((imageData) => {
			this.curImg.push('data:image/jpeg;base64,' + imageData);
			console.log("Image : " + this.curImg[0]);
			this.dataBase = this.curImg[0];
			this.sendRequestService();
		}, (err) => {
			// Handle error
			this.presentToast(err);
			console.log("Camera issue:" + err);
		});
	}

	sendRequestService(){
		this.apiService.requestServices("5d7a381aa4599", this.uid, "Test ...", this.curImg).subscribe(response => {
			console.log(response);
			if(response.status == "success"){
				this.presentToast("Yey, Layanan telah di buat ^_^ "+this.curImg[0]);
			}
		});
	}

	// for demo
	deleteAsd(val: any){
		// this.demoImg.pop();
		this.demoImg.splice(val, 1);
		// console.log(this.demoImg[val]);
	}

	deleteImg(index: any){
		this.curImg.splice(index, 1);
	}

}
