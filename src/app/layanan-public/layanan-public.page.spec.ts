import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayananPublicPage } from './layanan-public.page';

describe('LayananPublicPage', () => {
  let component: LayananPublicPage;
  let fixture: ComponentFixture<LayananPublicPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayananPublicPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayananPublicPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
