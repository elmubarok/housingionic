import { Component, OnInit } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ToastController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
	selector: 'app-layanan-home-lapor',
	templateUrl: './layanan-home-lapor.page.html',
	styleUrls: ['./layanan-home-lapor.page.scss'],
})
export class LayananHomeLaporPage implements OnInit {

	extrasData: any;
	curImg = [];

	constructor(
		public actionSheetController: ActionSheetController,
		private camera: Camera,
		public toastController: ToastController,
		private route: ActivatedRoute,
		private router: Router) {

		this.route.queryParams.subscribe(params => {
			if(this.router.getCurrentNavigation().extras.state){
				this.extrasData = this.router.getCurrentNavigation().extras.state.dataPage;
			}

			console.log(this.extrasData);
		});

	}

	ngOnInit() {
	}

	async presentToast(pict: string) {
		const toast = await this.toastController.create({
			message: pict,
			duration: 2000
		});
		toast.present();
	}

	async attachPic() {
		const actionSheet = await this.actionSheetController.create({
			header: 'Pilih',
			buttons: [{
				text: 'Take Picture',
				icon: 'camera',
				handler: () => {
					// act
					this.takePicture(this.camera.PictureSourceType.CAMERA);
				}
			}, {
				text: 'Phone Library',
				icon: 'photos',
				handler: () => {
					// act
					this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
				}
			}]
		});
		await actionSheet.present();
	}

	takePicture(sourceType){
		const options: CameraOptions = {
			quality: 100,
			sourceType: sourceType,
			destinationType: this.camera.DestinationType.DATA_URL,
			encodingType: this.camera.EncodingType.JPEG,
			mediaType: this.camera.MediaType.PICTURE
		};

		this.camera.getPicture(options).then((imageData) => {
			this.curImg.push('data:image/jpeg;base64,' + imageData);
		}, (err) => {
			// Handle error
			this.presentToast(err);
			console.log("Camera issue:" + err);
		});
	}

}
