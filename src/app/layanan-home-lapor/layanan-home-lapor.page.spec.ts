import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayananHomeLaporPage } from './layanan-home-lapor.page';

describe('LayananHomeLaporPage', () => {
  let component: LayananHomeLaporPage;
  let fixture: ComponentFixture<LayananHomeLaporPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayananHomeLaporPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayananHomeLaporPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
