import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { ListHome } from '../models/list-home';
import { LoginModel } from '../models/login';
import { LayananHome } from '../models/layanan-home';

import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})
export class ApiService {

	base_path = 'https://gardens.lyridsoftware.com/api/';

	constructor(private http: HttpClient) { }

	httpOptions = {
		headers: new HttpHeaders({
			'enctype': 'multipart/form-data;',
			'Content-Type': 'application/x-www-form-urlencoded'
		})
	}

	// Handle API errors
	handleError(error: HttpErrorResponse) {
		if (error.error instanceof ErrorEvent) {
			// A client-side or network error occurred. Handle it accordingly.
			console.error('An error occurred:', error.error.message);
		} else {
			// The backend returned an unsuccessful response code.
			// The response body may contain clues as to what went wrong,
			console.error(
				`Backend returned code ${error.status}, ` +
				`body was: ${error.error}`);
		}
		// return an observable with a user-facing error message
		return throwError(
			'Something bad happened; please try again later.');
	};

	getListHome(session: string): Observable<ListHome> {
		return this.http
		.post<ListHome>(this.base_path+"list_home", session, this.httpOptions)
		.pipe(
			retry(2),
			catchError(this.handleError)
			)
	}

	getLogin(token: string, email: string, password: string): Observable<LoginModel>{
		let data = "token="+token+"&email="+email+"&password="+password;
		return this.http
		.post<LoginModel>(this.base_path+"login", data, this.httpOptions)
		.pipe(
			retry(2),
			catchError(this.handleError)
			)
	}

	getLayananHome(session: string): Observable<LayananHome>{
		return this.http
		.post<LayananHome>(this.base_path+"service", session, this.httpOptions)
		.pipe(
			retry(2),
			catchError(this.handleError)
			)
	}

	requestServices(session: string, service: string, keluhan: string, photo: any){
		// let data = "session="+session+"&service="+service+"&keluhan="+keluhan+"&foto="+photo;
		let data = "session="+session+"&service="+service+"&keluhan="+keluhan;

		if(photo.length > 0){
			for(let i=0; i<photo.length; i++){
				data+="&foto[]="+photo[i];
			}
		}

		console.log(data);

		return this.http
		.post<LayananHome>(this.base_path+"add_layanan", data, this.httpOptions)
		.pipe(
			retry(2),
			catchError(this.handleError)
			)
	}

}
