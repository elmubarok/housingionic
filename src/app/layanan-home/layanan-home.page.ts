import { Component, OnInit } from '@angular/core';
import { Location } from "@angular/common";
import { ApiService } from '../services/api.service';
import { Router, NavigationExtras } from '@angular/router';

@Component({
	selector: 'app-layanan-home',
	templateUrl: './layanan-home.page.html',
	styleUrls: ['./layanan-home.page.scss'],
})
export class LayananHomePage implements OnInit {

	row1 = ['Light Bulb', 'Plumbing', 'Air Conditioner', 'Medicine', 'Laundry', 'Cleaning'];
	row2 = ['Medicine', 'Laundry', 'Cleaning'];
	row3 = ['Grocery', 'Phone Credits', 'General Repair'];

	layananData: any;

	constructor(
		private location: Location,
		public apiService: ApiService,
		private router: Router) {

		this.layananData = [];
	}

	ngOnInit() {
		this.initLayananData();
	}

	goToLapor(pageUid: string, pageTitle: string){
		let navigationExtras: NavigationExtras = {
			state: {
				dataPage: {
					uid: pageUid,
					title: pageTitle
				}
			}
		};
		this.router.navigate(['layanan-home-lapor'], navigationExtras);
	}

	initLayananData(){
		let session = "session=5d7a381aa4599";
		this.apiService.getLayananHome(session).subscribe(response => {
			this.layananData = response.result_home;
			this.layananData.pop();
			console.log(response);
		});
	}

}
