import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayananHomePage } from './layanan-home.page';

describe('LayananHomePage', () => {
  let component: LayananHomePage;
  let fixture: ComponentFixture<LayananHomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayananHomePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayananHomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
