import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiService } from '../services/api.service';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
	selector: 'app-tab1',
	templateUrl: 'tab1.page.html',
	styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

	cat: string = "new";
	slideDivWidth: any;
	slideDivHeight: any;

	listHomeDataUp: any;
	listHomeDataBot: any;

	// panicsess: any;
	panicsess: any;

	sliderOpts = {
		zoom: false,
		slidesPerView: 1.5,
		spaceBetween: 60,
		centeredSlides: true
	};

	constructor(
		public httpClient: HttpClient,
		public apiService: ApiService,
		private storage: Storage,
		private router: Router) {
		this.slideDivWidth = '246px';
		this.slideDivHeight = '254px';

		this.listHomeDataUp = [];
		this.listHomeDataBot = [];

		this.panicsess = [];
		// this.panicsess.push('null');

		this.storage.get('panic').then((val) => {
			this.panicsess[0] = val;
			console.log(val);
		});

		console.log("Panic: "+this.panicsess);

		// this.storage.get("session").then((val) => {
		// 	console.log("Session : " + val);
		// 	if(val == null){
		// 		this.router.navigate(['login']);
		// 	}
		// });
	}

	ngOnInit() {
		this.getAllList();
	}

	getAllList(){
		// let postdata = "session=";
		this.storage.get('session').then((val) => {
			if(val != null){
				let postdata = "session="+val;
				this.apiService.getListHome(postdata).subscribe(response => {
					console.log("Response : "+response.result_upper[0].uid);
					console.log("Session: "+val);
					console.log("COK: "+this.panicsess);
					this.listHomeDataUp = response.result_upper;
					this.listHomeDataBot = response.result_bottom;
				});
			}else{
				this.router.navigate(['login']);
			}
		});

		// this.apiService.getListHome(postdata).subscribe(response => {
		// 	console.log("Response : "+response.result_upper[0].uid);
		// 	this.listHomeDataUp = response.result_upper;
		// 	this.listHomeDataBot = response.result_bottom;
		// });
	}

	logout(){
		this.storage.clear();
		this.router.navigate(['login']);
		this.panicsess[0] = 'null';
	}

	checkPanicSess(){
		this.storage.set('panic', 'true').then(() => {
			this.storage.get('panic').then((val) => {
				this.panicsess[0] = val;
				console.log(val);
			});
		});
	}

}