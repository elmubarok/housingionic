import { Component } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
	selector: 'app-tab4',
	templateUrl: 'tab4.page.html',
	styleUrls: ['tab4.page.scss']
})
export class Tab4Page {

	statColor = ['#3ab54a','#00adef','#3ab54a','#3ab54a','#3ab54a','#3ab54a','#3ab54a'];

	constructor(
		public actionSheetController: ActionSheetController,
		private router: Router) {}

	async presentActionSheet() {
		const actionSheet = await this.actionSheetController.create({
			header: 'Pilih layanan',
			buttons: [{
				text: 'Home Service',
				handler: () => {
					this.openHome();
				}
			}, {
				text: 'Public Service',
				handler: () => {
					this.openPublic();
				}
			}]
		});
		await actionSheet.present();
	}

	openHome(){
		this.router.navigate(['layanan-home']);
	}

	openPublic(){
		this.router.navigate(['layanan-public']);
	}

}
