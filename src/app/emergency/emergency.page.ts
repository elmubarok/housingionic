import { Component, OnInit, Input, ViewChild, Output, EventEmitter, ElementRef } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({ 
	selector: 'app-emergency',
	templateUrl: './emergency.page.html',
	styleUrls: ['./emergency.page.scss'],
})
export class EmergencyPage{

	setIntID;
  // @Output() unlocked: EventEmitter<boolean> = new EventEmitter();
  // @ViewChild('unlock') input: any;
  // @ViewChild('range') range: any;
  // rangecok: rangecok = 20;
  // rangecok: {
  //   'lower': number
  // };

	constructor(
    public modalController: ModalController, 
    public httpClient: HttpClient,
    public alertController: AlertController) {}

	dismiss(){
		this.modalController.dismiss({
			'dismissed': true
		});
	}

	checkUnlock(evt: Event) {
    // let theRange = Number(this.input.nativeElement.value);
    // console.log(theRange);
    // if (evt.type == 'touchend') {
    //   if (theRange === 100) {
    //     alert("asd");
    //     this.input.nativeElement.value = 100;
    //     // this.unlockAction();
    //   } else {
    //     // this.setIntID = setInterval(() => {
    //     //   if (this.input.nativeElement.value > 0) {
    //     //     this.input.nativeElement.value = theRange--;
    //     //   } else {
    //     //     this.input.nativeElement.value = 0;
    //     //     this.unlocked.emit(false);
    //     //     clearInterval(this.setIntID);
    //     //   }
    //     // }, 0);
    //   }
    // } else {
    //   this.setIntID = setInterval(() => {
    //     if (this.input.nativeElement.value > 0) {
    //       this.input.nativeElement.value = theRange--;
    //     } else {
    //       this.input.nativeElement.value = 0;
    //       // this.unlocked.emit(false);
    //       // clearInterval(this.setIntID);
    //     }
    //   }, 1);
    // }
  }

  unlockAction(){
    console.log("confirmed");
  }

  onBlur(){
    // console.log(this.rangecok);
    // let curval = this.range.el.firstChild.value;
    // this.asd = 70;
    // if(curval >= 50){
    //   console.log(this.range.el.firstChild.value);
    //   // this.range.el.firstChild.value = 0;
    //   this.showAlert();
    // }else if(curval == 100){
    //   this.showAlert();
    // }
  }

  showAlert(){
    this.alertController.create({
      header: 'Confirmed',
      subHeader: 'Please tap OK to continue',
      backdropDismiss: false,
      buttons: ['OK']
    }).then(alert => alert.present());
  }

}
