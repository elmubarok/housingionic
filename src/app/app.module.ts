import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { EmergencyPage } from './emergency/emergency.page'
import { LayananHomePage } from './layanan-home/layanan-home.page'
import { LayananPublicPage } from './layanan-public/layanan-public.page'

import { HttpClientModule } from '@angular/common/http';

import { NgxSlideUnlockComponent } from 'ngx-slide-unlock';

import { Camera } from '@ionic-native/camera/ngx';
import { IonicStorageModule } from '@ionic/storage';

@NgModule({
  declarations: [
    AppComponent, 
    EmergencyPage, 
    NgxSlideUnlockComponent],
  entryComponents: [
    EmergencyPage],
  imports: [
  	FormsModule,
  	ReactiveFormsModule,
  	BrowserModule, 
  	IonicModule.forRoot(), 
  	AppRoutingModule, 
  	HttpClientModule,
    IonicStorageModule.forRoot()],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
