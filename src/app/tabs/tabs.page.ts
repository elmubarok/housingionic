import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { EmergencyPage } from '../emergency/emergency.page'

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  constructor(public modalController: ModalController) {}

  async presentEmergency(){
  	const modal = await this.modalController.create({
  		component: EmergencyPage
  	});
  	return await modal.present();
  }

}
