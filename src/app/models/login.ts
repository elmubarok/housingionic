export interface Cluster {
	uid: string;
	nama: string;
}

export interface Result {
	uid: string;
	nama: string;
	jabatan: string;
	user: string;
	email: string;
	emergency: string;
	cluster: Cluster[];
}

export interface LoginModel {
	status: string;
	token: string;
	session: string;
	result: Result[];
}