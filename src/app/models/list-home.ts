export interface ResultUpper {
    uid: string;
    title: string;
    sub_title: string;
    detail: string;
    image: string;
}

export interface ResultBottom {
    uid: string;
    date: string;
    category: string;
    title: string;
    sub_title: string;
    image: string;
}

export interface ListHome {
    status: string;
    result_upper: ResultUpper[];
    result_bottom: ResultBottom[];
}