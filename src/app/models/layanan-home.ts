export interface ResultPublic {
    uid: string;
    title: string;
    image: string;
}

export interface ResultHome {
    uid: string;
    title: string;
    image: string;
}

export interface LayananHome {
    status: string;
    result_public: ResultPublic[];
    result_home: ResultHome[];
}